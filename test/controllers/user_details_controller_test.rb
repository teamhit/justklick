require 'test_helper'

class UserDetailsControllerTest < ActionController::TestCase
  setup do
    @user_detail = user_details(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_details)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_detail" do
    assert_difference('UserDetail.count') do
      post :create, user_detail: { firstname: @user_detail.firstname, id_user: @user_detail.id_user, intercom_number: @user_detail.intercom_number, lastname: @user_detail.lastname, mobile: @user_detail.mobile, number_of_family_members: @user_detail.number_of_family_members, number_of_servants: @user_detail.number_of_servants, pets: @user_detail.pets, telephone: @user_detail.telephone, vehicle_number: @user_detail.vehicle_number }
    end

    assert_redirected_to user_detail_path(assigns(:user_detail))
  end

  test "should show user_detail" do
    get :show, id: @user_detail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_detail
    assert_response :success
  end

  test "should update user_detail" do
    patch :update, id: @user_detail, user_detail: { firstname: @user_detail.firstname, id_user: @user_detail.id_user, intercom_number: @user_detail.intercom_number, lastname: @user_detail.lastname, mobile: @user_detail.mobile, number_of_family_members: @user_detail.number_of_family_members, number_of_servants: @user_detail.number_of_servants, pets: @user_detail.pets, telephone: @user_detail.telephone, vehicle_number: @user_detail.vehicle_number }
    assert_redirected_to user_detail_path(assigns(:user_detail))
  end

  test "should destroy user_detail" do
    assert_difference('UserDetail.count', -1) do
      delete :destroy, id: @user_detail
    end

    assert_redirected_to user_details_path
  end
end
