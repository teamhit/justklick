json.array!(@user_details) do |user_detail|
  json.extract! user_detail, :id, :id_user, :firstname, :lastname, :telephone, :mobile, :number_of_family_members, :number_of_servants, :vehicle_number, :pets, :intercom_number
  json.url user_detail_url(user_detail, format: :json)
end
