json.array!(@societies) do |society|
  json.extract! society, :id, :name, :url, :building_number, :address, :area, :postcode, :city, :active, :IsDelete
  json.url society_url(society, format: :json)
end
