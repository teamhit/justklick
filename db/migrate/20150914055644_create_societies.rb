class CreateSocieties < ActiveRecord::Migration
  def change
    create_table :societies do |t|
      t.string :name
      t.string :url
      t.string :building_number
      t.string :address
      t.string :area
      t.integer :postcode
      t.string :city
      t.integer :active
      t.integer :IsDelete

      t.timestamps null: false
    end
    add_index :societies, :url, unique: true
  end
end
