class ChangemobileInUserDetails < ActiveRecord::Migration
  def change
	change_column :user_details, :mobile, :string, :limit=>10
  end
end
