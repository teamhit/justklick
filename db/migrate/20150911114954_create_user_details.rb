class CreateUserDetails < ActiveRecord::Migration
  def change
    create_table :user_details do |t|
      t.integer :id_user
      t.string :firstname
      t.string :lastname
      t.integer :telephone
      t.integer :mobile
      t.integer :number_of_family_members
      t.integer :number_of_servants
      t.string :vehicle_number
      t.integer :pets
      t.integer :intercom_number

      t.timestamps null: false
    end
  end
end
